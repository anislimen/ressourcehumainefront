import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models';

const AUTH_API = environment.baseURL;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  login(credentials): Observable<any> {
    return this.http.post(
      AUTH_API + 'login',
      {
        email: credentials.email,
        password: credentials.password,
      },
      httpOptions
    );
  }

  register(userDTO): Observable<any> {
    return this.http.post(AUTH_API + 'register', userDTO, httpOptions);
  }

  public signOut(): void {
    window.sessionStorage.clear();
  }

  public getUser(): Observable<User> {
    return of({
      name: 'John',
      lastName: 'Smith',
    });
  }
}
