import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services';

@Component({
  selector: 'app-sign-form',
  templateUrl: './sign-form.component.html',
  styleUrls: ['./sign-form.component.scss'],
})
export class SignFormComponent implements OnInit {
  isSuccessful = false;
  isSignUpFailed = false;
  showSpinner = false;
  errorMessage = '';

  //@Output() sendSignForm = new EventEmitter<void>();
  public form: FormGroup;
  constructor(private authService: AuthService) {}

  public ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
      ]),
      rpassword: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
      ]),
      nom: new FormControl('', [Validators.required, Validators.minLength(4)]),
      prenom: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ]),
      society: new FormControl('', [Validators.required]),
      activity: new FormControl('', [Validators.required]),
      adresse: new FormControl('', [Validators.required]),
      mobilePhone: new FormControl('', [Validators.required]),
    });
  }

  public sign(): void {
    if (this.form.valid) {
      this.showSpinner = true;
      // this.sendSignForm.emit();
      this.authService.register(this.form.value).subscribe(
        (data) => {
          this.showSpinner = false;
          this.form.reset();
          this.isSuccessful = true;
        },
        (err) => {
          this.errorMessage = err.error;
          this.isSignUpFailed = true;
          console.log(this.errorMessage);
        }
      );
    }
  }
}
