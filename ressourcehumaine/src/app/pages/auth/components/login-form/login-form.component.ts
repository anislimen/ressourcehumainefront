import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { routes } from 'src/app/consts';
import { AuthService, TokenStorageService } from '../../services';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  // @Output() sendLoginForm = new EventEmitter<void>();
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  public routers: typeof routes = routes;

  public form: FormGroup;
  constructor(
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private router: Router
  ) {}

  public ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });
  }

  public login(): void {
    if (this.form.valid) {
      //  this.sendLoginForm.emit();
      this.authService.login(this.form.value).subscribe(
        (data) => {
          this.tokenStorage.saveToken(data.jwt);
          this.tokenStorage.saveUser(data);

          this.isLoginFailed = false;
          this.isLoggedIn = true;
          //  window.location.replace("/dashboard");
          //  this.router.navigate(['/dashboard'])
          this.router.navigate([this.routers.DASHBOARD]).then();

          // this.roles = this.tokenStorage.getUser().roles;
        },
        (err) => {
          this.errorMessage = err.error.message;
          this.isLoginFailed = true;
        }
      );
    }
  }
}
