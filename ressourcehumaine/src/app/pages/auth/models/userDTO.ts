export class MUserDTO {
  constructor(
    public email: string = '',
    public password: string = '',
    public rpassword: string = '',
    public nom: string = '',
    public prenom: string = '',
    public society: string = '',
    public activity: string = '',
    public adresse: string = '',
    public mobilePhone: number = null
  ) {}
}
