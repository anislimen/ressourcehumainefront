import { Component, Input } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddUserComponent } from '..';

import { SupportRequestData } from '../../models/support-request-data';
import { MUtilisateur } from '../../models/utilisateur';
import { UtilisateurService } from '../../services';

@Component({
  selector: 'app-support-requests',
  templateUrl: './support-requests.component.html',
  styleUrls: ['./support-requests.component.scss'],
})
export class SupportRequestsComponent {
  @Input() supportRequestData: SupportRequestData[];
  @Input() users: MUtilisateur[];

  public displayedColumns: string[] = [
    'nom',
    'prenom',
    'adresse',
    'email',
    'action',
  ];

  constructor(
    private utilisateurService: UtilisateurService,
    public dialog: MatDialog
  ) {}

  onDelete(id) {
    console.log(id);
    this.utilisateurService.deleteUser(id).subscribe((data) => {
      this.utilisateurService.filter('Delete User');
    });
  }

  newUser() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = 900;

    const dialogRef = this.dialog.open(AddUserComponent, dialogConfig);
  }
}
