import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { UtilisateurService } from '../../services';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  //@Output() sendSignForm = new EventEmitter<void>();
  public form: FormGroup;
  constructor(
    private dialogRef: MatDialogRef<AddUserComponent>,
    private utilisateurService: UtilisateurService
  ) {}

  public ngOnInit(): void {
    this.form = new FormGroup({
      nom: new FormControl('', [Validators.required, Validators.minLength(4)]),
      prenom: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ]),
      adresse: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
      ]),
    });
  }

  public addUser(): void {
    if (this.form.valid) {
      // this.sendSignForm.emit();
      console.log(this.form.value);
      this.utilisateurService.addUser(this.form.value).subscribe(
        (data) => {
          this.utilisateurService.filter('New User');
          this.dialogRef.close();
          this.isSuccessful = true;
        },
        (err) => {
          this.errorMessage = err.error;
          this.isSignUpFailed = true;
          console.log(this.errorMessage);
        }
      );
    }
  }
}
