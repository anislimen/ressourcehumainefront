import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MUtilisateur } from '../models/utilisateur';
const API_URL = environment.baseURL;

@Injectable({
  providedIn: 'root',
})
export class UtilisateurService {
  constructor(private http: HttpClient) {}

  getAllUsers(): Observable<MUtilisateur[]> {
    return this.http.get<MUtilisateur[]>(API_URL + 'api/v1/user/allUsers');
  }

  deleteUser(id: number): Observable<any> {
    return this.http.get(API_URL + 'api/v1/user/delete/' + id);
  }

  addUser(userDTO): Observable<any> {
    return this.http.post(API_URL + 'api/v1/user/addUser', userDTO);
  }

  private _listners = new Subject<any>();
  listen(): Observable<any> {
    return this._listners.asObservable();
  }
  filter(filterBy: string) {
    this._listners.next(filterBy);
  }
}
