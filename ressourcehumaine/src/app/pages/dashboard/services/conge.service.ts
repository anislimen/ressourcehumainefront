import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
const API_URL = environment.baseURL;

@Injectable({
  providedIn: 'root',
})
export class CongeService {
  constructor(private http: HttpClient) {}

  getAllConge(): Observable<any> {
    return this.http.get<any[]>(API_URL + 'api/v1/');
  }
}
