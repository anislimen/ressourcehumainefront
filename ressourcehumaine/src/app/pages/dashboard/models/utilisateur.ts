import { MCompte } from './compte';
import { MRole } from './role';

export class MUtilisateur {
  id: number;
  Nom: string;
  Prenom: string;
  Adresse: string;
  email: string;
  password: string;
  status: Boolean;
  autorisation: boolean;
  compte: MCompte;
  role: MRole;
}
