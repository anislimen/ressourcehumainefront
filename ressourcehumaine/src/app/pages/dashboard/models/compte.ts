export class MCompte {
  id: number;
  society: string;
  activity: string;
  mobilePhone: number;
}
